# iris-dvc-demo

DVC demo using simple example (iris dataset).

For demonstration purposes we pretend that raw data has two files: one with sepal data and target, the other with "additional" petal data.

So, we have 4 stages:
1. Merge source file together and save them to parquet format.
2. Split to train/test.
3. Cross-validate and train a simple model (StandardScaler + LogReg).
4. Evaluate results on the test set.
