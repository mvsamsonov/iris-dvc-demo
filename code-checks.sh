#!/bin/bash
echo "Cd to pipeline folder"
pushd pipeline

echo "Running isort..."
isort .

echo "Running black..."
black .

echo "Running mypy..."
mypy --ignore-missing-imports .

echo "Cd back to root"
popd

echo "Running pytest..."
pytest
