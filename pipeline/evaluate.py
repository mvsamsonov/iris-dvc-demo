import argparse
import json

import dill
import pandas as pd
import yaml
from sklearn.metrics import accuracy_score

from utils.logs import get_logger


def evaluate(config_path: str) -> None:
    """Evaluate trained model"""

    with open(config_path or "params.yaml", "r") as f:
        config = yaml.safe_load(f)

    target_colname = config["general"]["target-colname"]
    test_path = config["split"]["test-filepath"]
    model_path = config["train"]["model-path"]
    drop_columns = config["train"]["drop-columns"]
    test_metrics_path = config["evaluate"]["test-metrics-path"]

    log_level = config["auxiliary"]["log-level"]
    logger = get_logger(__name__, log_level)

    logger.info("Loading test data")
    test = pd.read_parquet(test_path)

    logger.info("Loading model")
    with open(model_path, "rb") as fb:
        pipe = dill.load(fb)

    X_test = test.drop(drop_columns, axis=1).drop(target_colname, axis=1)
    y_test = test[target_colname]

    logger.info("Making prediction and evaluating")
    preds_test = pipe.predict(X_test)

    accuracy = accuracy_score(y_test, preds_test)
    logger.info(f"Test accuracy: {accuracy}")

    metrics = {
        "test-accuracy": accuracy,
    }

    with open(test_metrics_path, "w") as f:
        json.dump(metrics, f, indent=4)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path to params.yaml file")
    args = parser.parse_args()

    evaluate(args.config)
