import argparse
import json
from pathlib import Path

import dill
import pandas as pd
import yaml
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from utils.logs import get_logger


def train(config_path: str) -> None:
    """Do cross-validation and train model"""

    with open(config_path or "params.yaml", "r") as f:
        config = yaml.safe_load(f)

    random_state = config["general"]["random-state"]
    target_colname = config["general"]["target-colname"]
    train_path = config["split"]["train-filepath"]
    drop_columns = config["train"]["drop-columns"]
    logreg_params = config["train"]["logreg-params"]
    cross_val_params = config["train"]["cross-val-params"]
    cross_val_metrics_path = config["train"]["cross-val-metrics-path"]
    model_path = config["train"]["model-path"]

    log_level = config["auxiliary"]["log-level"]
    logger = get_logger(__name__, log_level)

    clf = LogisticRegression(**logreg_params, random_state=random_state)
    pipe = Pipeline([("scaler", StandardScaler()), ("clf", clf)])

    logger.info("Loading train data")
    df = pd.read_parquet(train_path)
    X = df.drop(drop_columns, axis=1).drop(target_colname, axis=1)
    y = df[target_colname]

    logger.info("Running cross-validation")
    accuracy_scores = cross_val_score(
        pipe,
        X,
        y,
        **cross_val_params,
    )

    logger.info(f"Cross-validation accuracy mean: {accuracy_scores.mean()}")
    logger.info(f"Cross-validation accuracy by fold: {accuracy_scores}")

    metrics = {
        "cross-val-accuracy": accuracy_scores.mean(),
        "cross-val-accuracy-folds": accuracy_scores.tolist(),
    }

    Path(cross_val_metrics_path).parent.mkdir(parents=True, exist_ok=True)
    with open(cross_val_metrics_path, "w") as f:
        json.dump(metrics, f, indent=4)

    logger.info("Training model")
    pipe.fit(X, y)

    logger.info(f"Saving model to {model_path}")
    Path(model_path).parent.mkdir(parents=True, exist_ok=True)
    with open(model_path, "wb") as fb:
        dill.dump(pipe, fb)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path to params.yaml file")
    args = parser.parse_args()

    train(args.config)
