import argparse

import pandas as pd
import yaml
from sklearn.model_selection import train_test_split

from utils.logs import get_logger


def split(config_path: str) -> None:
    """Split to train and test"""

    with open(config_path or "params.yaml", "r") as f:
        config = yaml.safe_load(f)

    random_state = config["general"]["random-state"]
    target_colname = config["general"]["target-colname"]

    source_data_path = config["prep"]["output-filepath"]
    train_path = config["split"]["train-filepath"]
    test_path = config["split"]["test-filepath"]
    test_size = config["split"]["test-size"]

    log_level = config["auxiliary"]["log-level"]
    logger = get_logger(__name__, log_level)

    logger.info("Loading source data")
    df = pd.read_parquet(source_data_path)

    logger.info("Splitting to train/test")
    train, test = train_test_split(
        df, test_size=test_size, random_state=random_state, stratify=df[target_colname]
    )

    logger.info(f"Saving to {train_path} and {test_path}")
    train.to_parquet(train_path, index=False)
    test.to_parquet(test_path, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path to params.yaml file")
    args = parser.parse_args()

    split(args.config)
