import argparse
import logging

import pandas as pd
import yaml

from utils.logs import get_logger


def prep(config_path: str) -> None:
    """Load all source files, create one file with all data, save to parquet"""

    with open(config_path or "params.yaml", "r") as f:
        config = yaml.safe_load(f)

    basic_filepath = config["prep"]["basic-filepath"]
    petal_filepath = config["prep"]["petal-filepath"]
    output_filepath = config["prep"]["output-filepath"]

    log_level = config["auxiliary"]["log-level"]
    logger = get_logger(__name__, log_level)

    logger.info("Loading and joining raw data")
    basic_df = pd.read_csv(basic_filepath)
    petal_df = pd.read_csv(petal_filepath)
    df = pd.merge(basic_df, petal_df, on="id", how="inner")

    logger.info(f"Saving result to {output_filepath}")
    df.to_parquet(output_filepath, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path to params.yaml file")
    args = parser.parse_args()

    prep(args.config)
